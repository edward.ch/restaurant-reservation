@extends('main.auth.app')
@section('title','Sign In')
@section('content')
    <!-- Sing in  Form -->
    <section class="sign-in" style="margin-top: -100px">
        <div class="container">
            <div class="signin-content">
                <div class="signin-image">
                    <figure><img src="{{asset('assets/form/images/signin-image.jpg')}}" alt="sing in image"></figure>
                    <a href="{{route('register')}}" class="signup-image-link">Create an account</a>
                    <a href="{{route('home')}}" class="signup-image-link">Back to home</a>
                </div>

                <div class="signin-form">
                    <h2 class="form-title">Sign in</h2>
                    <form method="POST" action="{{ route('login') }}" class="register-form" id="login-form">
                        @csrf
                        <div class="form-group">
                            <label for="email"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="email" id="email" placeholder="Your Email" name="email"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus/>
                        </div>
                            @error('email')
                            <span class="text-danger">
                                <strong style="color: red">{{ $message }}</strong>
                            </span><br>
                            @enderror
                        <div class="form-group">
                            <label for="password"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" id="password" placeholder="Password" name="password" required
                                   autocomplete="current-password"/>
                        </div>
                            @error('password')
                            <span class="text-danger">
                                <strong style="color: red">{{ $message }}</strong>
                            </span>
                            @enderror
                        <div class="form-group">
                            <input type="checkbox" class="agree-term" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }} />
                            <label for="remember" class="label-agree-term">
                                <span><span></span></span>
                                Remember me
                            </label>
                        </div>
                        <div class="form-group form-button">
                            <input type="submit" name="signin" id="signin" class="form-submit" value="Log in"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

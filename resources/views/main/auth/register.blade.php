@extends('main.auth.app')
@section('title','Sign Up')
@section('content')
    <!-- Sign up form -->
    <section class="signup" style="margin-top: -100px">
        <div class="container">
            <div class="signup-content">
                <div class="signup-form">
                    <h2 class="form-title">Sign up</h2>
                    <form method="POST" action="{{ route('register') }}" class="register-form" id="register-form">
                        @csrf
                        <div class="form-group">
                            <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="name" id="name" value="{{ old('name') }}" required
                                   autocomplete="name" autofocus placeholder="Your Name"/>
                        </div>
                        @error('name')
                        <span class="text-danger">
                            <strong style="color: red">{{ $message }}</strong>
                        </span><br>
                        @enderror
                        <div class="form-group">
                            <label for="email"><i class="zmdi zmdi-email"></i></label>
                            <input type="email" name="email" id="email" value="{{ old('email') }}" required
                                   autocomplete="email" placeholder="Your Email"/>
                        </div>
                        @error('email')
                        <span class="text-danger">
                            <strong style="color: red">{{ $message }}</strong>
                        </span><br>
                        @enderror
                        <div class="form-group">
                            <label for="password"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" name="password" id="password" required autocomplete="new-password"
                                   placeholder="Password"/>
                        </div>
                        @error('password')
                        <span class="text-danger">
                            <strong style="color: red">{{ $message }}</strong>
                        </span><br>
                        @enderror
                        <div class="form-group">
                            <label for="password-confirm"><i class="zmdi zmdi-lock-outline"></i></label>
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required autocomplete="new-password"
                                   placeholder="Repeat your password"/>
                        </div>
                        <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                        </div>
                    </form>
                </div>
                <div class="signup-image">
                    <figure><img src="{{asset('assets/form/images/signup-image.jpg')}}" alt="sing up image"></figure>
                    <a href="{{route('login')}}" class="signup-image-link">I am already member</a>
                    <a href="{{route('home')}}" class="signup-image-link">Back to home</a>
                </div>
            </div>
        </div>
    </section>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Restaurant Reservation - @yield('title') </title>
    <link rel="icon" href="{{asset('logo/icon.png')}}" type="image/x-icon">

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('assets/form/fonts/material-icon/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{asset('assets/form/css/style.css')}}">
</head>
<body>

<div class="main">
    @yield('content')
</div>

<!-- JS -->
<script src="{{asset('assets/form/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/form/js/main.js')}}"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>

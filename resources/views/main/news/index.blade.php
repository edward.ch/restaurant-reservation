@extends('main.app')

@section('title','- News')

@section('style')
@endsection

@section('content')
    <section class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 overlay-bg bg-image novi-background custom-bg-image bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">News</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li><a href="#">News</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-100 novi-background bg-cover">
        <div class="shell">
            <div class="range range-sm-center">
                <div class="col-sm-10 col-md-8 col-lg-12">
                    <div class="range range-30">

                        <div class="cell-lg-6">
                            <div class="post post-boxed">
                                <div class="post-media"><img class="img-responsive" src="{{asset('assets/images/blog-grid-1-570x321.jpg')}}" alt="" width="570" height="321"></div>
                                <div class="post-content text-left">
                                    <div class="post-tags group-xs"><a class="label-custom label-lg-custom label-primary" href="{{route('detail-news')}}">News</a></div>
                                    <div class="post-body">
                                        <div class="post-title">
                                            <h5><a class="link link-default link-white-sm" href="{{route('detail-news')}}">Example News</a></h5>
                                        </div>
                                        <div class="post-meta">
                                            <ul class="list-inline list-inline-sm p">
                                                <li class="text-middle"><span>by&nbsp;</span><span class="text-primary">Admin</span></li>
                                                <li><span class="text-middle novi-icon icon-xxs mdi mdi-clock"></span> <time class="text-middle text-primary-lighter" datetime="2018-01-01">22 oct 2019</time></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection

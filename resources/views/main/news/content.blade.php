@extends('main.app')

@section('title','- Example News')

@section('style')
@endsection

@section('content')
    <section class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 bg-image novi-background overlay-bg custom-bg-image bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">Example News</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li><a href="{{route('news')}}">News</a></li>
                        <li class="active">Detail</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-100 novi-background bg-cover bg-white">
        <div class="shell">
            <div class="range range-xs-center range-60 range-fix">
                <div class="cell-md-12">
                    <div class="section">
                        <article class="post post-classic single-post">
                            <img class="img-responsive" src="{{asset('assets/images/post-01-870x412.jpg')}}" alt="" width="870" height="412">
                            <div class="post-content text-left offset-top-25">
                                <h5 class="text-uppercase">Example News</h5>
                                <ul class="list-inline list-inline-md">
                                    <li>
                                        <div class="unit unit-horizontal unit-spacing-xxs">
                                            <div class="unit-left"><span class="text-base">Date:</span></div>
                                            <div class="unit-body"><time datetime="2018-01-01">22 Oct 2019</time></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="unit unit-horizontal unit-spacing-xxs">
                                            <div class="unit-left"><span class="text-base">Posted by:</span></div>
                                            <div class="unit-body"><a class="link link-gray-light" href="#">Admin</a></div>
                                        </div>
                                    </li>
                                </ul>
                                <hr>
                                <p class="text-base">At the very least, everyone has probably heard the name of the fourth Earl of Sandwich (born John Montagu), the British statesman whose name is forever affixed to our favorite lunchtime staple. What people may not know is that the foundations of this comfort food were laid long before the Earl's time.</p>
                                <p class="text-base">In fact, Arabs had already started stuffing meat inside pita bread centuries before the Earl came up with his delicious snack. He may not have been the first to eat meat between two slices of bread, but Sandwich did lend the now-famous food his name. According to one account, on Nov. 3, 1762, the Earl was deep into a marathon poker game and couldn't be bothered to leave the gaming table for dinner. As a solution, he asked a servant to bring him a piece of meat - stuffed between two slices of toast so he wouldn't smear food on his cards. Another story counters that it was actually work, rather than play, that kept the Earl from the dinner table. Either way, the meat-between-bread idea caught on, and it was dubbed the "sandwich" in the Earl's honor.</p>
                                <p class="text-base">Since then, the sandwich has branched out considerably. From its basic beginnings, it has expanded to include a variety of fillings and breads. Today, everything from salami on rye to filet mignon on ciabatta can claim the name "sandwich".</p>
                            </div>
                            <div class="offset-top-50 text-sm-left clearfix">
                                <div class="big text-bold text-base pull-sm-left">Share this post:</div>
                                <ul class="list-inline pull-sm-right offset-top-0 text-sm-right">
                                    <li><a class="link-darkest novi-icon icon icon-xxs-mod-1 fa fa-facebook" href="#"></a></li>
                                    <li><a class="link-darkest novi-icon icon icon-xxs-mod-1 fa fa-twitter" href="#"></a></li>
                                    <li><a class="link-darkest novi-icon icon icon-xxs-mod-1 fa fa-google-plus" href="#"></a></li>
                                    <li><a class="link-darkest novi-icon icon icon-xxs-mod-1 fa fa-pinterest" href="#"></a></li>
                                </ul>
                            </div>
                            <hr class="offset-top-50">
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection

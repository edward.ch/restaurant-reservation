@extends('main.app')
@section('content')
    <section
        class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 bg-image novi-background overlay-bg custom-bg-image bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">My Order</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li class="active">Order</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section
        class="text-center text-sm-left section-50 section-sm-top-80 section-sm-bottom-100 bg-gray-lighter novi-background bg-cover">
        <div class="shell">
            <h4 class="font-default text-center">Order</h4>
            <div class="range range-xs-center range-fix">
                <div class="cell-md-8">
                    <div class="inset-lg-left-45 inset-lg-right-45">
                        <!-- Bootstrap collapse-->
                        <div class="card-group-custom card-group-corporate" id="accordion1" role="tablist"
                             aria-multiselectable="false">
                            <!-- Bootstrap card-->
                            @foreach($order as $key=>$result)
                            <article class="card card-custom card-corporate">
                                <div class="card-header" id="accordion1Heading{{$key+1}}" role="tab">
                                    <div class="card-title"><a role="button" data-toggle="collapse"
                                                               data-parent="#accordion{{$key+1}}" href="#accordion{{$key+1}}Collapse{{$key+1}}"
                                                               aria-controls="accordion{{$key+1}}Collapse{{$key+1}}" aria-expanded="true">
                                            {{$result->date_reservation}} | <span class="badge badge-primary">{{$result->status}}</span>
                                            <div class="card-arrow"></div>
                                        </a></div>
                                </div>
                                <div class="collapse in" id="accordion1Collapse{{$key+1}}" role="tabpanel"
                                     aria-labelledby="accordion{{$key+1}}Heading{{$key+1}}">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                               <h4> Number Table : {{$result->tables->number_tables}}</h4>
                                            </div>
                                            <div class="col-lg-6"></div>
                                            <hr>
                                        </div>
                                       @foreach($basket->where('id_transactions',$result->id) as $value)
                                        <p>{{$value->quantity}}X <strong>{{$value->products->title}}</strong> ( Rp. {{$value->price}} )</p><hr>
                                       @endforeach
                                    </div>
                                </div>
                            </article><!-- Bootstrap card-->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

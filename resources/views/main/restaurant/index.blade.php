@extends('main.app')

@section('title','- List of Restaurant')

@section('style')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
@endsection

@section('content')
    <section
        class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 bg-image overlay-bg novi-background custom-bg-image bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">List of Restaurant - {{Request::is('restaurant') ? '' : $categoryRes->title }}</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li class="active">Restaurant</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-100 novi-background bg-cover">
        <div class="shell-wide">
            <div class="range range-xs-center range-100">
                <div class="cell-md-12 cell-xl-9">
                    <section>
                        <div class="shell">
                            <div class="range range-fix">
                                @foreach($restaurant as $result)
                                    <div class="cell-md-4 post-modern-wrap" style="margin-bottom: 30px">
                                        <article class="post post-modern">
                                            <div class="post-media">
                                                <img class="img-responsive img-cover"
                                                     src="{{asset('image/avatar/'.$result->image)}}"
                                                     alt="" style="width: 570px; height: 325px; object-fit: cover">
                                            </div>
                                            <div class="post-content text-left">
                                                <div class="post-meta pull-sm-right"><span
                                                        class="novi-icon text-middle icon-xxs mdi mdi-clock text-primary"></span>
                                                    <time class="text-italic text-middle" datetime="2018-01-01">
                                                        {{$result->open_hours}} - {{$result->close_hours}}
                                                    </time>
                                                </div>
                                                <ul class="list-inline offset-top-14 offset-sm-top-0">
                                                    <li>
                                                        <div class="post-tags group-xs">
                                                            <a class="label-custom label-sm-custom label-rounded-custom label-primary text-normal"
                                                               href="#">
                                                                {{$result->category->title}}
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="post-title">
                                                    <h6 class="offset-top-25">
                                                        <a class="link-default" href="{{route('detail-restaurant',$result->id)}}">
                                                            {{$result->name}}
                                                        </a>
                                                    </h6>
                                                    <div class="text-xs-right">
                                                        <a href="{{route('detail-restaurant',$result->id)}}" class="btn btn-primary">Book
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                @endforeach
                                <div class="range range-fix offset-top-60 offset-sm-top-100 range-xs-center">
                                    <div class="cell-md-8">
                                        <nav>
                                            <ul class="pagination-classic-variant-2">
                                                <li class="disabled"><span class="mdi mdi-chevron-double-left"></span>
                                                </li>
                                                <li class="active"><span>1</span></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a class="mdi mdi-chevron-double-right" href="#"></a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('script')
@endsection

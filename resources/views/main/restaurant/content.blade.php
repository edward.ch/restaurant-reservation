@extends('main.app')

@section('title','- Restaurant 1')

@section('style')
    <style>
        * {box-sizing: border-box;}
        body {font-family: Verdana, sans-serif;}
        .mySlides {display: none;}
        img {vertical-align: middle;}

        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
        }

        .active-slide {
            background-color: #717171;
        }

        /* Fading animation */
        .fades {
            -webkit-animation-name: fades;
            -webkit-animation-duration: 1.5s;
            animation-name: fades;
            animation-duration: 1.5s;
        }

        @-webkit-keyframes fades {
            from {opacity: .4}
            to {opacity: 1}
        }

        @keyframes fades {
            from {opacity: .4}
            to {opacity: 1}
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
            .text {font-size: 11px}
        }
    </style>
@endsection

@section('content')
    <section
        class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 bg-image overlay-bg novi-background custom-bg-image bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">{{$restaurant->name}}</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li><a href="{{route('restaurant')}}">Restaurant</a></li>
                        <li class="active">Detail</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section
        class="text-center text-sm-left section-50 section-sm-top-100 section-sm-bottom-100 bg-image-1 novi-background custom-bg-image">
        <div class="shell">
            @if (session('alert'))
            <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> {{ session('alert') }}
            </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Alert!</strong> {{ session('warning') }}
                </div>
            @endif
            <div class="range range-30 range-xs-center">
                <div class="cell-md-7">
                    <div class="deals-block" >
                    <div class="slideshow-container">

                        <div class="mySlides fades">
                            <div class="numbertext">1 / 3</div>
                            <img src="https://americana-group.com/app/uploads/2017/11/koki1920x960-1048x524.jpg" style="width:100%; height: 480px">
                            <div class="text">Caption Text</div>
                        </div>

                        <div class="mySlides fades">
                            <div class="numbertext">2 / 3</div>
                            <img src="https://www.aspens-services.co.uk/wp-content/uploads/2019/01/World-Kitchen-web.jpg" style="width:100%;  height: 480px">
                            <div class="text">Caption Two</div>
                        </div>

                        <div style="text-align:center; display: none">
                            <span class="dot"></span>
                            <span class="dot"></span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="cell-md-5">
                    <div class="range range-30">
                        <div class="cell-sm-12">
                            <div style="width: 100%">
                                <iframe width="100%" height="220"
                                        src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q={{$restaurant->gmap_query}}+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a
                                        href="https://www.maps.ie/coordinates.html">latitude longitude finder</a>
                                </iframe>
                            </div>
                        </div>
                        <div class="cell-sm-12">
                            <div class="deals-block deals-block-discount novi-background deals-block-without-price">
                                <div class="caption">
                                    <div class="title-wrap">
                                        <h4 class="text-italic">Go To Checkout</h4>
                                        <p>Food in basket ( {{Cart::content()->count()}} )</p>
                                    </div>
                                    <div class="discount-block"><i style="color: white; margin: 5px"
                                                                   class="fa fa-shopping-basket"></i></div>
                                    <div class="offset-top-15">
                                        @auth
                                        <a class="btn btn-burnt-sienna btn-shape-circle" href="{{route('checkout-first',$restaurant->id)}}">
                                            Go To Checkout
                                        </a>
                                        @else
                                        <button type="button" data-target="#restaurantModal"
                                                data-toggle="modal" class="btn btn-burnt-sienna btn-shape-circle">Book Now
                                        </button>
                                        @endauth
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            @foreach($categoryProduct as $data)
            <div class="title-wrap">
                <h4 class="text-italic" style="color: white">{{$data->title}}</h4>
            </div>
            <div class="range range-xs-center offset-top-30 range-30">
                @foreach($product->where('id_category',$data->id) as $result)
                <div class="cell-sm-6 cell-md-4">
                    <a class="deals-block deals-block-small" href="{{route('add-cart',$result->id)}}">
                        <img class="img-responsive" src="{{asset('image/product/'.$result->image)}}" style="width: 369px;height: 294px; object-fit: cover" alt=""
                            width="369" height="294">
                        <div class="caption">
                            <div class="title-wrap">
                                <h4 class="text-italic">{{$result->title}}</h4>
                                <p><strong>Rp. {{$result->price}}</strong></p>
                                <p>{{$result->desc}}</p>
                            </div>
                            <div class="price-block novi-background"><span>Add </span><span></span></div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </section>

    @auth
        <!-- Modal -->
        <div class="modal fade" id="restaurantModal" role="dialog" aria-labelledby="restaurantModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="restaurantModalLabel">Alert!!</h5>
                    </div>
                    <div class="modal-body">
                        <p>You must be logged in</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a href="{{route('login')}}" class="btn btn-primary">Login</a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <!-- Modal -->
        <div class="modal fade" id="restaurantModal" role="dialog" aria-labelledby="restaurantModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="restaurantModalLabel">Alert!!</h5>
                    </div>
                    <div class="modal-body">
                        <p>You must be logged in</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a href="{{route('login')}}" class="btn btn-primary">Login</a>
                    </div>
                </div>
            </div>
        </div>
    @endauth
@endsection

@section('script')
    <script>
        var slideIndex = 0;
        showSlides();

        function showSlides() {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > slides.length) {slideIndex = 1}
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            setTimeout(showSlides, 4000);
        }
    </script>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $('#datetimepicker1').datetimepicker({
            defaultDate: new Date(),
            format: 'DD/MM/YYYY',
            sideBySide: true
        });
    </script>

@endsection

@extends('main.app')

@section('title','- Checkout')

@section('style')
@endsection

@section('content')
    <section
        class="text-center text-sm-left section-50 section-sm-top-80 section-sm-bottom-100 bg-gray-lighter novi-background bg-cover">
        <div class="shell">
            <h4 class="font-default text-center">{{$menu == '1' ? 'Choose Your Date & Time' : $menu == '2' ? 'Choose Your Table' : 'Checkout'}}</h4>
            <div class="range range-xs-center range-fix">
                @if (session('warning'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Alert!</strong> {{ session('warning') }}
                    </div>
                @endif
                @if($menu == '1')
                    <div class="cell-md-12">
                        <div class="inset-lg-left-45 inset-lg-right-45">
                            <!-- Bootstrap collapse-->
                            <div class="card-group-custom card-group-corporate" id="accordion2" role="tablist"
                                 aria-multiselectable="false">
                                <!-- Bootstrap card-->
                                <article class="card card-custom card-corporate">
                                    <div class="card-header" id="accordion1Heading2" role="tab">
                                        <div class="card-title"><a role="button" data-toggle="collapse"
                                                                   data-parent="#accordion2" href="#accordion1Collapse2"
                                                                   aria-controls="accordion1Collapse1"
                                                                   aria-expanded="true">
                                                Date & Time
                                                <div class="card-arrow"></div>
                                            </a></div>
                                    </div>
                                    <div class="collapse in" id="accordion1Collapse2" role="tabpanel"
                                         aria-labelledby="accordion1Heading1">
                                        <div class="card-body" style="background-color: #f7f7f7">
                                            <div class="container">
                                                <form action="{{route('filter-table',$id)}}" method="POST">
                                                    @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class='input-group date' id='datetimepicker1'>
                                                                <input type='text' name="date" class="form-control"/>
                                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <div class='input-group'>
                                                                <select style="color: white" name="time" class="form-control" id="exampleFormControlSelect1">
                                                                    <option>- Choose the Time -</option>
                                                                    @foreach($time as $result)
                                                                        <option value="{{$result->id}}">{{$result->time}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><br>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="{{route('detail-restaurant',$id)}}" class="btn btn-primary">Cancel</a>
                                                    </div>
                                                    <div class="col-md-6"></div>
                                                    <div class="col-md-2">
                                                        <button type="submit" class="btn btn-primary">Next</button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </article><!-- Bootstrap card-->
                            </div>
                        </div>
                    </div>
                @elseif($menu == '2')
                    <div class="cell-md-12">
                        <div class="inset-lg-left-45 inset-lg-right-45">
                            <!-- Bootstrap collapse-->
                            <div class="card-group-custom card-group-corporate" id="accordion2" role="tablist"
                                 aria-multiselectable="false">
                                <!-- Bootstrap card-->
                                <article class="card card-custom card-corporate">
                                    <div class="card-header" id="accordion1Heading2" role="tab">
                                        <div class="card-title"><a role="button" data-toggle="collapse"
                                                                   data-parent="#accordion2" href="#accordion1Collapse2"
                                                                   aria-controls="accordion1Collapse1"
                                                                   aria-expanded="true">
                                                Tables
                                                <div class="card-arrow"></div>
                                            </a></div>
                                    </div>
                                    <div class="collapse in" id="accordion1Collapse2" role="tabpanel"
                                         aria-labelledby="accordion1Heading1">
                                        <div class="card-body" style="background-color: #f7f7f7">
                                            <form action="{{route('get-table',$id)}}">
                                                @csrf

                                                <div class="row">

                                                    <input type="hidden" name="date" value="{{Session::get('date')}}">
                                                    <input type="hidden" name="time" value="{{Session::get('time')}}">
                                                    <div class="form-group">
                                                        <div class='input-group'>
                                                            <select style="color: white" name="table" class="form-control" id="exampleFormControlSelect1">
                                                                <option>- Choose the Table -</option>
                                                                @foreach(Session::get('table') as $result)
                                                                    <option value="{{$result->id}}">Number Table :{{$result->number_tables}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                            <br><br>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="{{route('checkout-first',$id)}}" class="btn btn-primary">Back</a>
                                                </div>
                                                <div class="col-md-8"></div>
                                                <div class="col-md-2">
                                                    <button type="submit" class="btn btn-primary">Next</button>
                                                </div>
                                            </div>
                                            </form>

                                        </div>
                                    </div>
                                </article><!-- Bootstrap card-->
                            </div>
                        </div>
                    </div>
                @else
                    <div class="cell-md-8">
                        <div class="inset-lg-left-45 inset-lg-right-45">
                            <!-- Bootstrap collapse-->
                            <div class="card-group-custom card-group-corporate" id="accordion1" role="tablist"
                                 aria-multiselectable="false">
                                <!-- Bootstrap card-->
                                <article class="card card-custom card-corporate">
                                    <div class="card-header" id="accordion1Heading1" role="tab">
                                        <div class="card-title"><a role="button" data-toggle="collapse"
                                                                   data-parent="#accordion1" href="#accordion1Collapse1"
                                                                   aria-controls="accordion1Collapse1"
                                                                   aria-expanded="true">
                                                Order Detail
                                                <div class="card-arrow"></div>
                                            </a></div>
                                    </div>
                                    <div class="collapse in" id="accordion1Collapse1" role="tabpanel"
                                         aria-labelledby="accordion1Heading1">
                                        <div class="card-body" style="background-color: #f7f7f7">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6>
                                                        Number Table {{$transaction->tables->number_tables}}
                                                    </h6>
                                                </div>
                                                <div class="col-md-6">
                                                    <h7>{{$transaction->date_reservation}}</h7>
                                                </div>
                                            </div>
                                            <hr>
                                            <form id="form-transaction"method="POST" action="{{route('create-reservation',$transaction->id)}}">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="form-label form-label-outside" for="name">Full
                                                        Name</label>
                                                    <input class="form-control" id="name" type="text"
                                                           placeholder="Your Full Name" name="name" value="{{Auth::user()->name}}"
                                                           data-constraints="@Required">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label form-label-outside"
                                                           for="email">Email</label>
                                                    <input class="form-control" id="email" type="email"
                                                           placeholder="Your Email" name="email" value="{{Auth::user()->email}}"
                                                           data-constraints="@Required" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label form-label-outside"
                                                           for="email">Phone Number</label>
                                                    <input class="form-control" id="email" type="number"
                                                           placeholder="Your Phone Number" name="phone"
                                                           data-constraints="@Required">
                                                </div>
                                                <div class="form-group offset-top-15"><label
                                                        class="form-label form-label-outside" for="message">your
                                                        Notes</label> <textarea class="form-control" id="message"
                                                                                  placeholder="Write your notes here"
                                                                                  name="notes"
                                                                                  data-constraints="@Required"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Book</button>
                                                <button href="{{route('cancel-reservation',$transaction->id)}}" class="btn btn-secondary">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </article><!-- Bootstrap card-->
                            </div>
                        </div>
                    </div>
                    <div class="cell-md-4">
                        <div class="inset-lg-left-45 inset-lg-right-45">
                            <!-- Bootstrap collapse-->
                            <div class="card-group-custom card-group-corporate" id="accordion2" role="tablist"
                                 aria-multiselectable="false">
                                <!-- Bootstrap card-->
                                <article class="card card-custom card-corporate">
                                    <div class="card-header" id="accordion1Heading2" role="tab">
                                        <div class="card-title"><a role="button" data-toggle="collapse"
                                                                   data-parent="#accordion2" href="#accordion1Collapse2"
                                                                   aria-controls="accordion1Collapse1"
                                                                   aria-expanded="true">
                                                Cart
                                                <div class="card-arrow"></div>
                                            </a></div>
                                    </div>
                                    <div class="collapse in" id="accordion1Collapse2" role="tabpanel"
                                         aria-labelledby="accordion1Heading1">
                                        <div class="card-body" style="background-color: #f7f7f7">
                                            <h6>Item ( {{Cart::content()->count()}} )</h6>
                                            <hr>
                                            <div class="card-body">
                                                <div class="row">
                                                    @foreach(Cart::content() as $value)
                                                    <div class="col-md-9">
                                                        <h7>
                                                            <strong>{{$value->name}}</strong>
                                                            <small>Rp.{{$value->price}} - {{$value->qty}}x</small>
                                                        </h7>
                                                    </div>
                                                        <div class="col-md-2">
                                                            <a href="{{route('remove-cart',$value->rowId)}}"><i style="color: red" class="fa fa-remove"></i></a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <br>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h6>Total:</h6>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <h7>Rp.{{Cart::total()}}</h7>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article><!-- Bootstrap card-->
                            </div>
                        </div>
                    </div>
                @endauth
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script
        src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $('#datetimepicker1').datetimepicker({
            defaultDate: new Date(),
            format: 'DD/MM/YYYY',
            sideBySide: true
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        $("#form-product").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-transaction")[0]);

            $.ajax({
                url: $("#form-transaction").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
            })
                .done(function (data) {
                    Swal.fire(
                        'Saved!',
                        'Your Reservation has been created',
                        'success'
                    ).then(function () {
                        location.href = '{{route('home')}}'
                    })
                })
                .fail(function () {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!'
                    })
                })

        })
    </script>
@endsection


@extends('main.app')

@section('title','- Profile')

@section('style')
@endsection

@section('content')
    <section
        class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 overlay-bg bg-image novi-background custom-bg-image bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">My Profile</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li class="active">My Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-top-90 section-sm-bottom-100 novi-background bg-cover bg-white">
        <div class="shell">
            <div class="range range-50 range-lg-60 range-xs-center offset-top-45 offset-lg-top-80">
                <div class="cell-xs-6 cell-md-4">
                    <div class="thumbnail-variant-4"><img src="{{asset('image/avatar/'.Auth::user()->image)}}" alt=""
                                                          width="250" height="250">

                    </div>
                </div>
                <div class="cell-xs-6 cell-md-4">
                    <form class="rd-mailform form-contact-line text-left offset-top-35"
                          data-form-output="form-output-global" data-form-type="contact" method="post"
                          action="bat/rd-mailform.php">
                        <div class="form-inline-flex">
                            <div class="form-group">
                                <label class="form-label form-label-outside" for="contact-name">Full Name</label>
                                <input class="form-control" id="contact-name" type="text" placeholder="Your Full Name" name="name"
                                       value="{{Auth::user()->name}}" data-constraints="@Required">
                            </div>
                        </div>
                        <div class="form-inline-flex">
                            <div class="form-group">
                                <label class="form-label form-label-outside" for="contact-email">Email</label>
                                <input class="form-control" id="contact-email" type="email" placeholder="Your Email" name="email"
                                       value="{{Auth::user()->email}}" data-constraints="@Required" disabled>
                            </div>
                        </div>

                        <div class="offset-top-15">
                            <div class="form-inline-flex">
                                <div class="form-group">
                                    <a href="{{route('logout')}}" class="btn btn-primary-lighter btn-fullwidth" >
                                        Logout
                                    </a>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary-lighter btn-fullwidth" type="submit">
                                       Update Profile
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection

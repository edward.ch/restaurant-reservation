<!DOCTYPE html>
<html class="wide wow-animation" lang="en">

<head>
    <title>Restaurant Reservation @yield('title')</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport"
          content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1,user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{{asset('logo/icon.png')}}" type="image/x-icon">

    @yield('style')

    <link rel="stylesheet" type="text/css"
          href="//fonts.googleapis.com/css?family=Changa+One:400,400i%7CGrand+Hotel%7CLato:300,400,400italic,700">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/novi.css')}}">
</head>

<body>
<div class="page-loader">
    <div class="page-loader-body">
        <div class="cssload-container">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
    </div>
</div>
<div class="page text-center">
    @include('main.layouts.navbar')

    @yield('content')

    @include('main.layouts.footer')
</div>
<div class="snackbars" id="form-output-global"></div>
@yield('script')

<script src="{{asset('assets/js/core.min.js')}}"></script>
<script src="{{asset('assets/js/script.js')}}"></script>
<script src='{{asset('assets/js/html5shiv.min.js')}}'></script>

</body>
</html>

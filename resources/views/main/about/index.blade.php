@extends('main.app')

@section('title','- About Us')

@section('style')
@endsection

@section('content')
    <section class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 bg-image novi-background custom-bg-image overlay-bg bg-image-breadcrumbs">
        <div class="shell shell-fluid">
            <div class="range range-condensed range-fix">
                <div class="cell-xs-12 cell-xl-12">
                    <p class="h3 text-white">About Us</p>
                    <ul class="breadcrumbs-custom offset-top-10">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li class="active">About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="text-center text-sm-left section-50 section-sm-top-100 section-sm-bottom-205 section-with-angle bg-black-haze novi-background">
        <div class="shell">
            <div class="range range-xs-center range-md-right range-fix">
                <div class="cell-sm-10 cell-md-6">
                    <h4 class="font-default">&nbsp;About Restaurant Reservation</h4>
                    <p>Restoral, one of today’s most renowned fast food restaurants, was established in 2013, as a quiet place where anyone could eat a burger or have a pizza that they loved. Our dedication to customers and quality food helps us to reach more today.</p>
                    <div class="range range-xs-center offset-top-30 text-center">
                        <div class="cell-xs-6 cell-sm-4">
                            <div class="counter text-primary">20</div>
                            <div class="counter-header">
                                <p>table booked</p>
                            </div>
                        </div>
                        <div class="cell-xs-6 cell-sm-4">
                            <div class="counter text-primary">100</div>
                            <div class="counter-header">
                                <p>transaction</p>
                            </div>
                        </div>
                        <div class="cell-xs-6 cell-sm-4">
                            <div class="counter text-primary">64</div>
                            <div class="counter-header">
                                <p>restaurant partner</p>
                            </div>
                        </div>
                    </div><a class="btn btn-shape-circle btn-burnt-sienna offset-top-40" href="#">See our restaurant</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-top-90 section-sm-bottom-100 novi-background bg-cover">
        <div class="shell">
            <h4 class="font-default">Our Story</h4>
            <div class="range range-xs-center range-fix">
                <div class="cell-md-10">
                    <div class="timeline text-center">
                        <div class="timeline-item-wrap">
                            <div class="timeline-date"><span class="h5">2014</span></div>
                            <div class="range range-sm-reverse range-sm-middle timeline-item timeline-item-reverse range-fix">
                                <div class="cell-sm-6">
                                    <h5>Idea</h5>
                                    <p class="body-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consectetur, quisquam voluptates architecto nam, quaerat dicta harum exercitationem, nulla debitis optio dolorum quia accusantium, soluta totam.</p>
                                </div>
                                <div class="cell-sm-6"><img class="img-responsive" src="images/about-01-420x259.jpg" alt="" width="420" height="259"></div>
                            </div>
                        </div>
                        <div class="timeline-item-wrap">
                            <div class="timeline-date"><span class="h5">2013</span></div>
                            <div class="range range-sm-middle timeline-item range-fix">
                                <div class="cell-sm-6"><img class="img-responsive" src="images/about-02-420x259.jpg" alt="" width="420" height="259"></div>
                                <div class="cell-sm-6">
                                    <h5>Opening</h5>
                                    <p class="body-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consectetur, quisquam voluptates architecto nam, quaerat dicta harum exercitationem, nulla debitis optio dolorum quia accusantium, soluta totam.</p>
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-wrap">
                            <div class="timeline-date"><span class="h5">2019</span></div>
                            <div class="range range-sm-reverse range-sm-middle timeline-item timeline-item-reverse range-fix">
                                <div class="cell-sm-6">
                                    <h5>Today</h5>
                                    <p class="body-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consectetur, quisquam voluptates architecto nam, quaerat dicta harum exercitationem, nulla debitis optio dolorum quia accusantium, soluta totam.</p>
                                </div>
                                <div class="cell-sm-6"><img class="img-responsive" src="images/about-03-420x259.jpg" alt="" width="420" height="259"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection

<header class="page-head">
    <div class="rd-navbar-wrap rd-navbar-minimal">
        <nav class="rd-navbar novi-background bg-cover" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
             data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static"
             data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static"
             data-xxl-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="100px"
             data-lg-stick-up-offset="100px">
            <div class="shell shell-fluid">
                <div class="rd-navbar-inner">
                    <div class="rd-navbar-panel">
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span>
                        </button>
                        <a class="rd-navbar-brand brand" href="{{route('home')}}">
                            <div class="brand-logo">
                                <img class="brand-logo-dark" src="{{asset('logo/Logo_Black_ex.png')}}" alt="" width="280"
                                     height="60">
                                <img class="brand-logo-light" src="{{asset('logo/Logo_White_ex.png')}}" alt="" width="280"
                                     height="60">
                            </div>
                        </a></div>
                    <div class="rd-navbar-nav-wrap">
                        <ul class="rd-navbar-nav">
                            <li class="{{Request::is('/') ? 'active' : ''}}">
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            <li class="{{Request::is('about-us') ? 'active' : ''}}">
                                <a href="{{route('about')}}">About Us</a>
                            </li>
                            <li class="{{Request::is('restaurant')}} rd-navbar--has-dropdown rd-navbar-submenu">
                                <a href="{{route('restaurant')}}">Restaurant</a>
                                <ul class="rd-navbar-dropdown">
                                    @foreach($category as $result)
                                        <li><a href="{{route('category-list-restaurant',$result->id)}}">{{$result->title}}</a></li>
                                    @endforeach
                                    <li><a href="{{route('restaurant')}}">All</a></li>
                                </ul>
                            </li>
                            <li class="{{Request::is('news') ? 'active' : ''}}">
                                <a href="{{route('news')}}">News</a>
                            </li>
                            @auth
                                <li class="{{Request::url() === '/profile' ? 'active' : ''}} rd-navbar--has-dropdown rd-navbar-submenu">
                                    <a href="#">My Profile <i class="fa fa-circle" style="color: red"></i> </a>
                                    <ul class="rd-navbar-dropdown">
                                        <li><a href="{{route('order')}}">Order <i class="fa fa-circle" style="color: red"></i></a></li>
{{--                                        <li><a href="#">History Order</a></li>--}}
                                        <li><a href="{{route('profile')}}">Setting Profile</a></li>
                                    </ul>
                                </li>
                            @endauth
                        </ul>
                        <ul class="rd-navbar-aside-right list-inline">
                            @auth
                                <li>
                                    <div class="unit unit-horizontal unit-middle unit-spacing-xxs link-default" href="{{ route('login') }}">
                                        <div class="unit-body">
                                            <address class="contact-info">
                                                <span class="text-bold big">Hy. {{ Auth::user()->name }} </span>
                                            </address>
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li>
                                    <a class="unit unit-horizontal unit-middle unit-spacing-xxs link-default" href="{{ route('login') }}">
                                        <div class="unit-body">
                                            <address class="contact-info"><span class="text-bold big">Login </span>
                                            </address>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="unit unit-horizontal unit-middle unit-spacing-xxs link-default" href="{{ route('register') }}">
                                        <div class="unit-body">
                                            <address class="contact-info"><span class="text-bold big"> Register</span>
                                            </address>
                                        </div>
                                    </a>
                                </li>
                            @endauth
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>

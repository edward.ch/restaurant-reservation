<footer class="page-foot text-sm-left">
    <div class="bg-gray-darker section-top-50 section-bottom-60 novi-background bg-cover">
        <div class="shell">
            <div class="range border-left-cell range-50">
                <div class="cell-sm-6 cell-md-5 cell-lg-4">
                    <a class="brand brand-inverse reveal-inline-block"
                       href="index.html">
                        <img class="brand-logo-dark" src="{{asset('logo/Logo_Black_ex.png')}}" alt="" width="280" height="60">
                        <img class="brand-logo-light" src="{{asset('logo/Logo_White_ex.png')}}" alt="" width="280" height="60">
                    </a>
                    <p class=" contact-info offset-top-20 mr-left-20">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Sint esse, ullam officiis a? Officiis fugiat consectetur deleniti, illum iste
                        electus, nemo est?</p>
                    <ul class="list-unstyled contact-info offset-top-20">
                        <li>
                            <div class="unit unit-sm-horizontal unit-spacing-xxs">
                                <div class="unit-left"><span class="text-white">Email:</span></div>
                                <div class="unit-body"><a class="link-gray-light" href="mailto:#">info@demolink.org</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="cell-sm-6 cell-md-3  cell-lg-4">
                    <h4 class="text-uppercase">Restaurant Category</h4>
                    <ul class="list-tags offset-top-15">
                        @foreach($category as $result)
                            <li class="text-gray-light"><a class="link-gray-light" href="{{route('category-list-restaurant',$result->id )}}">{{$result->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="cell-sm-10 cell-lg-4 cell-md-4">
                    <h4 class="text-uppercase">Follow Us</h4>
                    <div class="offset-top-20">
                        <ul class="list-inline">
                            <li><a class="novi-icon icon icon-xs link-gray-light fa-facebook" href="#"></a></li>
                            <li><a class="novi-icon icon icon-xs link-gray-light fa-twitter" href="#"></a></li>
                            <li><a class="novi-icon icon icon-xs link-gray-light fa-google-plus" href="#"></a></li>
                            <li><a class="novi-icon icon icon-xs link-gray-light fa-instagram" href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-20 bg-gray-darker border-top novi-background bg-cover">
        <div class="shell">
            <div class="range range-30 range-xs-center range-sm-justify range-fix">
                <div class="cell-sm-5 text-md-left">
                    <p class="copyright">Restaurant Reservation&nbsp;©<span class="copyright-year">2019</span>
                </div>
                <div class="cell-sm-4 text-md-right">
                    <ul class="list-inline list-inline-sizing-1">
                        <li><a class="link-silver-light" href="#"><span
                                    class="novi-icon icon icon-xs fa-instagram"></span></a></li>
                        <li><a class="link-silver-light" href="#"><span
                                    class="novi-icon icon icon-xs fa-facebook"></span></a></li>
                        <li><a class="link-silver-light" href="#"><span
                                    class="novi-icon icon icon-xs fa-twitter"></span></a></li>
                        <li><a class="link-silver-light" href="#"><span
                                    class="novi-icon icon icon-xs fa-google-plus"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

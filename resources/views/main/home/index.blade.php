@extends('main.app')

@section('title','')

@section('style')
@endsection

@section('content')
    <section class="bg-gray-darker">
        <div class="swiper-variant-1">
            <div class="swiper-container swiper-slider" data-slide-effect="fade" data-autoplay="5000" data-min-height="600px">
                <div class="swiper-wrapper">
                    @foreach($slider as $result)
                        <div class="swiper-slide" data-slide-bg="{{asset('image/slider/'.$result->image)}}">
                            <div class="swiper-slide-caption slide-caption-2 text-center">
                                <div class="shell">
                                    <div class="range range-sm-middle">
                                        <div class="cell-sm-6 cell-md-5 cell-lg-4">
                                            <h5 class="text-italic font-secondary text-white" data-caption-animate="fadeInUpSmall" data-caption-delay="100">------</h5>
                                            <h1 class="text-white offset-top-0" data-caption-animate="fadeInUpSmall" data-caption-delay="700">{{$result->title}}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination-wrap">
                    <div class="shell">
                        <div class="range range-fix">
                            <div class="cell-sm-6 cell-md-5 cell-lg-4">
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="text-center text-sm-left section-50 section-sm-top-100 section-sm-bottom-205 section-with-angle bg-black-haze novi-background">
        <div class="shell">
            <div class="range range-xs-center range-md-right range-fix">
                <div class="cell-sm-10 cell-md-6">
                    <h4 class="font-default">&nbsp;About Restaurant Reservation</h4>
                    <p>Restoral, one of today’s most renowned fast food restaurants, was established in 2013, as a quiet place where anyone could eat a burger or have a pizza that they loved. Our dedication to customers and quality food helps us to reach more today.</p>
                    <div class="range range-xs-center offset-top-30 text-center">
                        <div class="cell-xs-6 cell-sm-4">
                            <div class="counter text-primary">20</div>
                            <div class="counter-header">
                                <p>table booked</p>
                            </div>
                        </div>
                        <div class="cell-xs-6 cell-sm-4">
                            <div class="counter text-primary">100</div>
                            <div class="counter-header">
                                <p>transaction</p>
                            </div>
                        </div>
                        <div class="cell-xs-6 cell-sm-4">
                            <div class="counter text-primary">64</div>
                            <div class="counter-header">
                                <p>restaurant partner</p>
                            </div>
                        </div>
                    </div><a class="btn btn-shape-circle btn-burnt-sienna offset-top-40" href="#">See our restaurant</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-100 novi-background bg-cover bg-white">
        <div class="shell">
            <div class="range range-xs-center range-50">
                <div class="cell-sm-6 cell-md-3 view-animate fadeInUpBigger delay-04">
                    <article class="box-icon box-icon-variant-1">
                        <div class="icon-wrap"><span class="novi-icon icon icon-lg text-base thin-icon-time icon-xl"></span></div>
                        <div class="box-icon-header">
                            <h5>Fast Booking</h5>
                        </div>
                        <hr class="divider-xs bg-primary">
                        <p>Everything you order at Restaurant Reservation will be quickly booked.</p>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-3 view-animate fadeInUpBigger delay-08">
                    <article class="box-icon box-icon-variant-1">
                        <div class="icon-wrap"><span class="novi-icon icon icon-lg text-base restaurant-icon-17 icon-xxl"></span></div>
                        <div class="box-icon-header">
                            <h5>fresh food</h5>
                        </div>
                        <hr class="divider-xs bg-primary">
                        <p>We use only the best ingredients to cook the tasty fresh food for you.</p>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-3 view-animate fadeInUpBigger delay-04">
                    <article class="box-icon box-icon-variant-1">
                        <div class="icon-wrap"><span class="novi-icon icon icon-lg text-base restaurant-icon-23 icon-xxl"></span></div>
                        <div class="box-icon-header">
                            <h5>best restaurant</h5>
                        </div>
                        <hr class="divider-xs bg-primary">
                        <p>Our restaurant partner has staff consists of chefs and cooks with years of experience.</p>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-3 view-animate fadeInUpBigger delay-06">
                    <article class="box-icon box-icon-variant-1">
                        <div class="icon-wrap"><span class="novi-icon icon icon-lg text-base restaurant-icon-22 icon-xxl"></span></div>
                        <div class="box-icon-header">
                            <h5>a variety of dishes</h5>
                        </div>
                        <hr class="divider-xs bg-primary">
                        <p>In our menu you’ll find a wide variety of dishes, desserts, and drinks.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="section-50 section-sm-top-90 section-sm-bottom-100 novi-background bg-cover bg-white">
        <div class="shell">
            <h4 class="font-default">The Newest Restaurant</h4>
            <div class="owl-carousel owl-carousel-1 offset-top-45" data-items="1" data-sm-items="2" data-md-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
                @foreach($restaurant as $result)
                    <div class="cell-md-4 post-modern-wrap">
                        <article class="post post-modern">
                            <div class="post-media">
                                <img class="img-responsive img-cover"
                                     src="{{asset('image/avatar/'.$result->image)}}"
                                     alt="" style="width: 570px; height: 325px; object-fit: cover">
                            </div>
                            <div class="post-content text-left">
                                <div class="post-meta pull-sm-right"><span
                                        class="novi-icon text-middle icon-xxs mdi mdi-clock text-primary"></span>
                                    <time class="text-italic text-middle" datetime="2018-01-01">
                                        {{$result->open_hours}} - {{$result->close_hours}}
                                    </time>
                                </div>
                                <ul class="list-inline offset-top-14 offset-sm-top-0">
                                    <li>
                                        <div class="post-tags group-xs">
                                            <a class="label-custom label-sm-custom label-rounded-custom label-primary text-normal"
                                               href="#">
                                                {{$result->category->title}}
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="post-title">
                                    <h6 class="offset-top-25">
                                        <a class="link-default" href="{{route('detail-restaurant',$result->id)}}">
                                            {{$result->name}}
                                        </a>
                                    </h6>
                                    <div class="text-xs-right">
                                        <a href="{{route('detail-restaurant',$result->id)}}" class="btn btn-primary">Book
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection

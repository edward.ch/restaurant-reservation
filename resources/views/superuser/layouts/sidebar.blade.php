<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout"
                        data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="link-effect font-w700" href="">
                        <i class="text-primary"></i>
                        <span class="font-size-xl text-dual-primary-dark">Resto</span><span
                            class="font-size-xl text-primary">Reservation</span>
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{asset('image/avatar/'.Auth::user()->image)}}" alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="#">
                    <img class="img-avatar" src="{{asset('image/avatar/'.Auth::user()->image)}}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                           href="#">{{Auth::user()->name}}</a>
                    </li>
                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">
                @if(Auth::user()->role == 'admin')
                    <li>
                        <a class="{{Request::is('admin/dashboard') ? 'active' : ''}}"
                           href="{{route('dashboard-admin')}}"><i class="si si-home"></i><span
                                class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">User</span></li>
                    <li class="{{Request::is('admin/restaurant') || Request::is('admin/category') ? 'open' : ''}}">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-list"></i><span
                                class="sidebar-mini-hide">Restaurant Partner</span></a>
                        <ul>
                            <li>
                                <a class="{{Request::is('admin/restaurant') ? 'active' : ''}}"
                                   href="{{route('restaurant-admin')}}"><span class="sidebar-mini-hide">List Restaurant</span></a>
                            </li>
                            <li>
                                <a class="{{Request::is('admin/category') ? 'active' : ''}}"
                                   href="{{route('category-admin')}}"><span class="sidebar-mini-hide">List Category</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="{{Request::is('admin/user') ? 'active' : ''}}" href="{{route('user-admin')}}"><i
                                class="si si-user"></i><span class="sidebar-mini-hide">User</span></a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Website Configuration</span></li>
                    <li>
                        <a class="{{Request::is('admin/slider') ? 'active' : ''}}" href="{{route('slider-admin')}}"><i
                                class="si si-picture"></i><span class="sidebar-mini-hide">Slider</span></a>
                    </li>
                    <li>
                        <a class="{{Request::is('admin/news') ? 'active' : ''}}" href="{{route('news-admin')}}"><i
                                class="si si-docs"></i><span class="sidebar-mini-hide">News</span></a>
                    </li>
                    <li>
                        <a class="{{Request::is('') ? 'active' : ''}}" href=""><i
                                class="si si-info"></i><span class="sidebar-mini-hide">Information</span></a>
                    </li>
                @elseif(Auth::user()->role == 'restaurant')
                    <li>
                        <a class="{{Request::is('partner/dashboard') ? 'active' : ''}}"
                           href="{{route('dashboard-restaurant')}}"><i class="si si-home"></i><span
                                class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Product</span></li>
                    <li class="{{Request::is('partner/product') || Request::is('partner/category') ? 'open' : ''}}">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-handbag"></i><span
                                class="sidebar-mini-hide">Product</span></a>
                        <ul>
                            <li>
                                <a class="{{Request::is('partner/product') ? 'active' : ''}}"
                                   href="{{route('product-restaurant')}}"><span class="sidebar-mini-hide">List Product</span></a>
                            </li>
                            <li>
                                <a class="{{Request::is('partner/category') ? 'active' : ''}}"
                                   href="{{route('category-restaurant')}}"><span class="sidebar-mini-hide">List Category</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="{{Request::is('partner/tables') ? 'active' : ''}}" href="{{route('tables-restaurant')}}"><i
                                class="si si-tag"></i><span class="sidebar-mini-hide">Tables</span></a>
                    </li>
                    <li>
                        <a class="{{Request::is('partner/time') ? 'active' : ''}}" href="{{route('time-restaurant')}}"><i
                                class="si si-clock"></i><span class="sidebar-mini-hide">Time Reservation</span></a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Order</span></li>
                    <li>
                        <a class="{{Request::is('partner/order') ? 'active' : ''}}" href="{{route('order-restaurant')}}"><i
                                class="si si-docs"></i><span class="sidebar-mini-hide">Order <span class="badge badge-danger">{{$order->count()}}</span></span></a>
                    </li>
                    <li>
                        <a class="{{Request::is('partner/history/transaction') ? 'active' : ''}}" href="{{route('history-restaurant')}}"><i
                                class="si si-chart"></i><span class="sidebar-mini-hide">History Transaction</span></a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Configuration</span></li>
                    <li>
                        <a class="{{Request::is('partner/slider') ? 'active' : ''}}" href="{{route('slider-restaurant')}}"><i
                                class="si si-picture"></i><span class="sidebar-mini-hide">Slider</span></a>
                    </li>
                    <li>
                        <a class="{{Request::is('parnter/configuration') ? 'active' : ''}}" href="{{route('configuration-restaurant')}}"><i
                                class="si si-settings"></i><span class="sidebar-mini-hide">Configuration</span></a>
                    </li>
                @else
                    -
                @endif
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>
<!-- END Sidebar -->

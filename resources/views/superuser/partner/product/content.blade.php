@extends('superuser.app')

@section('title','Create Product')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h1 class="h2 font-w700 text-white mb-10">Create new Product</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="{{route('product-restaurant')}}">Product</a>
                <span class="breadcrumb-item active">Create</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <!-- Bootstrap Design -->
        <div class="row">
            <div class="col-md-12">
                <!-- Default Elements -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Product</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form id="form-product" action="{{route('created-product-restaurant')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-2" for="text-input">Title <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="text-input"
                                           name="title" placeholder="Title of product.." required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="select-input">Category <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control" id="select-input" name="category" required>
                                        <option>- Select a category -</option>
                                        @foreach($category as $result)
                                            <option value="{{$result->id}}">{{$result->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="price-input">Price <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Rp</div>
                                        </div>
                                        <input type="number" min="0" class="form-control" name="price" id="price-input"
                                               placeholder="Price of product.." required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="desc-input">Description <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="desc" id="desc-input" rows="3"
                                              required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="example-text-input">Image <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-2">
                                    <img src="http://placehold.it/100x100" id="showgambar"
                                         style="max-width:200px;max-height:200px;float:left;"/>
                                </div>
                                <div class="col-lg-8">
                                    <input type="file" id="inputgambar" name="image" class="validate"/>
                                </div>
                            </div>
                            <div style="margin-bottom: 20px; margin-top: 20px">
                                <button type="submit" class="btn btn-primary"> Submit</button>
                                <a href="{{route('product-restaurant')}}">CANCEL</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        $("#form-product").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-product")[0]);

            $.ajax({
                url: $("#form-product").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
            })
                .done(function (data) {
                    Swal.fire(
                        'Saved!',
                        'Your Product has been created',
                        'success'
                    ).then(function () {
                        location.href = '{{route('product-restaurant')}}'
                    })
                })
                .fail(function () {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!'
                    })
                })

        })
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showgambar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#inputgambar').change(function () {
                readURL(this);
            });

        }
    </script>
@endsection

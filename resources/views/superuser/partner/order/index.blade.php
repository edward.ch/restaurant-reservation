@extends('superuser.app')

@section('title','List Product')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h1 class="h2 font-w700 text-white mb-10">List Order</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="#">Order</a>
                <span class="breadcrumb-item active">List</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List <small>Order</small></h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th style="width: 10%" class="text-center">No</th>
                        <th>Date Reservation</th>
                        <th>Order Name</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 15%;">Actions</th>
                        <th class="text-center" style="width: 15%;">Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order as $key => $result)
                        <tr>
                            <td class="text-center">{{$key + 1}}</td>
                            <td class="font-w600">{{$result->date_reservation}}</td>
                            <td class="font-w600">{{$result->name}}</td>
                            <td class="font-w600">Rp. {{$result->total}}</td>
                            <td class="text-center">
                                {{$result->status}}
                            </td>
                            <td class="text-center">
                                <button id="btn-accept" class="btn btn-sm btn-success" data-toggle="tooltip"
                                        data-url="{{route('order-accept-restaurant',$result->id)}}" title="Accept">
                                    <i style="color: white" class="fa fa-check"></i>
                                </button>
                                <a class="btn btn-sm btn-danger" href="{{route('order-remove-restaurant',$result->id)}}" title="Remove">
                                    <i style="color: white" class="fa fa-remove"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a class="btn btn-sm btn-primary" href="#" title="Detail">
                                    <i style="color: white" class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@section('script')
    <script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page JS Code -->
    <script src="{{asset('assets/js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script type="text/javascript">
        $(document).on("click", "#btn-accept", function () {
            var url = $(this).data('url');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't accept this order!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Accept!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Order has been Accepted.',
                        'success'
                    ).then(function () {
                        location.href = url
                    })
                }
            })
        });
    </script>
@endsection

@extends('superuser.app')

@section('title','Dashboard')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('superuser_assets/assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h2 class="h2 font-w700 text-white mb-10">Dashboard</h2>
                    <h4 class="h4 font-w400 text-white-op mb-0">Welcome {{ auth()->user()->name }}</h4>
                    <hr>
                    <h5 id="time" class="h5 font-w400 text-white-op mb-0">10.20</h5>
                    <h5 id="date" class="h5 font-w400 text-white-op mb-0">Sunday, 10 sepetmber 1029</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <span class="breadcrumb-item active">Dashboard</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <!-- Statistics -->
        <!-- CountTo ([data-toggle="countTo"] is initialized in Helpers.coreAppearCountTo()) -->
        <!-- For more info and examples you can check out https://github.com/mhuggins/jquery-countTo -->
        <div class="row gutters-tiny">
            <!-- Earnings -->
            <div class="col-md-6 col-xl-3">
                <a class="block block-rounded block-transparent bg-gd-elegance" href="{{route('order-restaurant')}}">
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-area-chart text-white-op"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-white" data-toggle="countTo" data-to="{{$order->count()}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-white-op">Order</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END Earnings -->

            <!-- Orders -->
            <div class="col-md-6 col-xl-3">
                <a class="block block-rounded block-transparent bg-gd-dusk" href="">
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket text-white-op"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-white" data-toggle="countTo" data-to="{{$transaction->count()}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-white-op">Transaction</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END Orders -->

            <!-- New Customers -->
            <div class="col-md-6 col-xl-3">
                <a class="block block-rounded block-transparent bg-gd-sea" href="{{route('product-restaurant')}}">
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-archive text-white-op"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-white" data-toggle="countTo" data-to="{{$product->count()}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-white-op">Product</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END New Customers -->

            <!-- Conversion Rate -->
            <div class="col-md-6 col-xl-3">
                <a class="block block-rounded block-transparent bg-gd-aqua" target="_blank" href="{{route('home')}}">
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-cart-arrow-down text-white-op"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-white"><i class="fa fa-arrow-right"></i></div>
                            <div class="font-size-sm font-w600 text-uppercase text-white-op">Visit Site</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END Conversion Rate -->
        </div>
        <!-- END Statistics -->
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        var d = new Date();
        var months = [
            "January","February","March","April","May","June","July","August","September","October","November","December"
        ];
        var days = [
            "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"
        ];

        document.getElementById("time").innerHTML = d.getHours() + ":" + d.getMinutes();
        document.getElementById("date").innerHTML = days[d.getDay()] + ", " + d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();
    </script>
@endsection

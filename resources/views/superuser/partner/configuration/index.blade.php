@extends('superuser.app')

@section('title','Create Product')

@section('style')
    <link rel="stylesheet" href="{{asset('superuser_assets/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h1 class="h2 font-w700 text-white mb-10">Create new Restaurant Partner</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="">Partner</a>
                <span class="breadcrumb-item active">Create</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <!-- Bootstrap Design -->
        <div class="row">
            <div class="col-md-12">
                <!-- Default Elements -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Restaurant Partner</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form id="form-product" action="{{route('update-config-restaurant')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-2" for="selectCategory">Category Restaurant <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control" id="selectCategory" name="category" required>
                                        @foreach($category as $result)
                                            <option value="{{$result->id}}" {{$result->id == Auth::user()->id_category ? 'selected' : ''}}>{{$result->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="example-text-input">Open Hours <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="text" value="{{$user->open_hours}}" class="js-masked-time form-control" id="example-masked-time" name="open" placeholder="00:00">
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" value="{{$user->close_hours}}" class="js-masked-time form-control" id="example-masked-time" name="close" placeholder="00:00">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="example-text-input">Location <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10" style="margin-bottom: 20px">
                                    <input type="text" name="gmap" class="form-control" id="us3-address" required/>
                                </div>

                                <label class="col-lg-2" for="example-text-input">Map <span class="text-danger">*</span></label>
                                <div class="col-lg-10" style="margin-bottom: 20px">
                                    <div id="us3" style="width: 100%; height: 400px;"></div>
                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-2">
                                    <label class=" control-label">Radius:</label>
                                    <input type="text" class="form-control" id="us3-radius"/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="p-r-small control-label">Lat.:</label>
                                    <input type="text" name="latitude" class="form-control" style="width: 100%" id="us3-lat"/>
                                </div>
                                <div class="col-lg-4">
                                    <label class="p-r-small control-label">Long.:</label>
                                    <input type="text" name="longitude" class="form-control" style="width: 100%" id="us3-lon"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="example-text-input">Image <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-2">
                                    <img src="{{asset('image/avatar/'.$user->image)}}" id="showgambar"
                                         style="max-width:150px;max-height:150px;float:left;"/>
                                </div>
                                <div class="col-lg-8">
                                    <input type="file" id="inputgambar" name="image" class="validate"/>
                                </div>
                            </div>
                            <div style="margin-bottom: 20px; margin-top: 20px">
                                <button type="submit" class="btn btn-primary"> Submit</button>
                                <a href="{{route('restaurant-admin')}}">CANCEL</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('script')
    <script src="{{asset('superuser_assets/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        $("#form-product").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-product")[0]);

            $.ajax({
                url: $("#form-product").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
            })
                .done(function (data) {
                    Swal.fire(
                        'Saved!',
                        'Your configuration has been updated',
                        'success'
                    ).then(function () {
                        location.href = '{{route('configuration-restaurant')}}'
                    })
                })
                .fail(function () {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!'
                    })
                })

        })
    </script>
    <script type="text/javascript"
            src='https://maps.google.com/maps/api/js?key=AIzaSyBy_G0H8GDS521QaVAYPk_pinqKLqRdj3M&sensor=false&libraries=places'></script>
    <script src="{{asset('locationpicker.jquery.min.js')}}"></script>
    <script>
        $('#us3').locationpicker({
            location: {
                latitude: '{{$user->latitude}}',
                longitude: '{{$user->longitude}}'
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us3-lat'),
                longitudeInput: $('#us3-lon'),
                radiusInput: $('#us3-radius'),
                locationNameInput: $('#us3-address')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });
    </script>
@endsection

@extends('superuser.app')

@section('title','List Product')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h1 class="h2 font-w700 text-white mb-10">List Tables</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="#">Tables</a>
                <span class="breadcrumb-item active">List</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List <small>Tables</small></h3>
                <div class="block-options">
                    <a href="{{route('create-tables-restaurant')}}" class="btn btn-primary">Create New <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th style="width: 10%" class="text-center">No</th>
                        <th style="width: 15%">Number Tables</th>
                        <th style="width: 25%">Title</th>
                        <th class="text-center" style="width: 15%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tables as $key => $result)
                        <tr>
                            <td class="text-center">{{$key + 1}}</td>
                            <td class="font-w600">Number : {{$result->number_tables}}</td>
                            <td class="font-w600">{{$result->title}}</td>
                            <td class="text-center">
                                <a class="btn btn-sm btn-primary" href="#" title="Update">
                                    <i style="color: white" class="fa fa-pencil"></i>
                                </a>
                                <button id="btn-remove" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Delete"
                                        data-url="#">
                                    <i style="color: white" class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@section('script')
    <script src="{{asset('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page JS Code -->
    <script src="{{asset('assets/js/pages/be_tables_datatables.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script type="text/javascript">
        $(document).on("click", "#btn-remove", function () {
            var url = $(this).data('url');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't clear this item!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Your item has been deleted.',
                        'success'
                    ).then(function () {
                        location.href = url
                    })
                }
            })
        });
    </script>
@endsection

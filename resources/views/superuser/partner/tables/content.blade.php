@extends('superuser.app')

@section('title','Create Product')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h1 class="h2 font-w700 text-white mb-10">Create new Tables</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="{{route('tables-restaurant')}}">Tables</a>
                <span class="breadcrumb-item active">Create</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <!-- Bootstrap Design -->
        <div class="row">
            <div class="col-md-12">
                <!-- Default Elements -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Product</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form id="form-product" action="{{route('created-tables-restaurant')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-2" for="number-input">Number Table <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Number</div>
                                        </div>
                                        <input type="number" min="1" class="form-control" name="number" id="number-input"
                                               placeholder="Number of table.." required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="text-input">Title <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="text-input"
                                           name="title" placeholder="Title of table.." required>
                                </div>
                            </div>
                            <div style="margin-bottom: 20px; margin-top: 20px">
                                <button type="submit" class="btn btn-primary"> Submit</button>
                                <a href="{{route('tables-restaurant')}}">CANCEL</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        $("#form-product").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-product")[0]);

            $.ajax({
                url: $("#form-product").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
            })
                .done(function (data) {
                    Swal.fire(
                        'Saved!',
                        'Your Table has been created',
                        'success'
                    ).then(function () {
                        location.href = '{{route('tables-restaurant')}}'
                    })
                })
                .fail(function () {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!'
                    })
                })

        })
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showgambar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#inputgambar').change(function () {
                readURL(this);
            });

        }
    </script>
@endsection

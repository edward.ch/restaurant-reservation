@extends('superuser.app')

@section('title','Create Slider')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{asset('assets/media/photos/photo26@2x.jpg')}}');">
        <div class="bg-black-op-75">
            <div class="content content-top content-full text-center">
                <div class="py-20">
                    <h1 class="h2 font-w700 text-white mb-10">Create new Slider</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Breadcrumb -->
    <div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="{{route('slider-admin')}}">Slider</a>
                <span class="breadcrumb-item active">Create</span>
            </nav>
        </div>
    </div>
    <!-- END Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <!-- Bootstrap Design -->
        <div class="row">
            <div class="col-md-12">
                <!-- Default Elements -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">News</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form id="form-product" action="{{route('created-slider-admin')}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-2" for="text-input">Title <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="text-input"
                                           name="title" placeholder="Title of slider.." required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2" for="example-text-input">Image <span
                                        class="text-danger">*</span></label>
                                <div class="col-lg-2">
                                    <img src="http://placehold.it/100x100" id="showgambar"
                                         style="max-width:200px;max-height:200px;float:left;"/>
                                </div>
                                <div class="col-lg-8">
                                    <input type="file" id="inputgambar" name="image" class="validate"/>
                                </div>
                            </div>
                            <div style="margin-bottom: 20px; margin-top: 20px">
                                <button type="submit" class="btn btn-primary"> Submit</button>
                                <a href="{{route('slider-admin')}}">CANCEL</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        $("#form-product").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-product")[0]);

            $.ajax({
                url: $("#form-product").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
            })
                .done(function (data) {
                    Swal.fire(
                        'Saved!',
                        'Your Slider has been created',
                        'success'
                    ).then(function () {
                        location.href = '{{route('slider-admin')}}'
                    })
                })
                .fail(function () {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!'
                    })
                })

        })
    </script>
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}
    <script type="text/javascript">
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showgambar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#inputgambar').change(function () {
                readURL(this);
            });

        }
    </script>
@endsection

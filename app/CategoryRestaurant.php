<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryRestaurant extends Model
{
    protected $table = 'category_restaurants';

    public function user(){
        return $this->belongsTo('App\User');
    }
}

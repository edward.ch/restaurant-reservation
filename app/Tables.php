<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    protected $table = 'tables';

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }
}

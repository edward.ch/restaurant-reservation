<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateTransaction extends Model
{
    protected $table = 'date_transactions';
}

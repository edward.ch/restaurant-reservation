<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeReservation extends Model
{
    protected $table = 'time_reservations';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderRestaurants extends Model
{
    protected $table = 'slider_restaurants';
}

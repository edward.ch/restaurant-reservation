<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $table = 'baskets';

    public function products(){
        return $this->hasOne('App\Product', 'id', 'id_products');
    }
}

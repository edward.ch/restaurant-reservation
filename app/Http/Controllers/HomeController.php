<?php

namespace App\Http\Controllers;

use App\CategoryProduct;
use App\CategoryRestaurant;
use App\Product;
use App\Slider;
use App\User;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller{

    public function load(){
        if (Auth::user()->role == 'admin') {
            return redirect()->route('dashboard-admin');
        } elseif (Auth::user()->role == 'restaurant'){
            return redirect()->route('dashboard-restaurant');
        } elseif (Auth::user()->role == 'user'){
            return redirect()->route('home');
        } else{
            abort(404);
        }
        return redirect()->route('home');
    }

    public function index(){
        $data['category'] = CategoryRestaurant::all();
        $data['slider'] = Slider::all();
        $data['restaurant'] = User::where('role','restaurant')->take(3)->latest()->get();
        return view('main.home.index',$data);
    }

    public function about(){
        $data['category'] = CategoryRestaurant::all();
        return view('main.about.index',$data);
    }

    public function listRestaurant(){
        $data['category'] = CategoryRestaurant::all();
        $data['restaurant'] = User::where('role','restaurant')->get();
        return view('main.restaurant.index',$data);
    }

    public function categoryRestaurant($id){
        $data['category'] = CategoryRestaurant::all();
        $data['categoryRes'] = CategoryRestaurant::find($id);
        $data['restaurant'] = User::where('role','restaurant')->where('id_category',$id)->get();
        return view('main.restaurant.index',$data);
    }

    public function detailRestaurant($id){
        $data['category'] = CategoryRestaurant::all();
        $data['restaurant'] = User::find($id);
        $data['product'] = Product::where('id_restaurants',$id)->get();
        $data['categoryProduct'] = CategoryProduct::where('id_restaurants',$id)->get();

        return view('main.restaurant.content',$data);
    }

    public function listNews(){
        $data['category'] = CategoryRestaurant::all();
        return view('main.news.index',$data);
    }

    public function detailNews(){
        $data['category'] = CategoryRestaurant::all();
        return view('main.news.content',$data);
    }

    public function profile(){
        $data['category'] = CategoryRestaurant::all();
        return view('main.profile.index',$data);
    }

}

<?php

namespace App\Http\Controllers;

use App\Basket;
use App\CategoryRestaurant;
use App\DateTransaction;
use App\Product;
use App\Tables;
use App\TimeReservation;
use App\Transaction;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addCart($id)
    {
        $product = Product::find($id);

        $data = array(
            'id' => $id,
            'name' => $product->title,
            'qty' => 1,
            'price' => $product->price,
            'weight' => 0
        );

        Cart::add($data);

        return redirect()->back()->with('alert', $product->title . ' 1x, added in cart');

    }

    public function removeCart($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back();

    }

    public function chooseDate($id)
    {

        if (Cart::count() == 0) {
            return redirect()->back()->with('warning', 'You Must Add a Product');
        }

        $data['menu'] = '1';
        $data['category'] = CategoryRestaurant::all();
        $data['time'] = TimeReservation::where('id_restaurants', $id)->get();
        $data['id'] = $id;

        return view('main.checkout.index', $data);
    }

    public function filterTable($id, Request $request)
    {

        if ($request->date <= date('d/n/Y') ) {
            return redirect()->back()->with('warning', 'Date can`t be used');
        }

        $time_reservation = TimeReservation::find($request->time);
        $dateTime = $request->date . " - " . $time_reservation->time;
        
        if (Transaction::where('date_reservation',$dateTime)->first() == null){
            $data['table'] = Tables::where('id_restaurants', $id)->get();
        }else{
            $id_table = Transaction::where('date_reservation',$dateTime)->first()->id_tables;
            $data['table'] = Tables::where('id_restaurants', $id)->where('id','!=',$id_table)->get();
        }

        $data['time'] = $request->time;
        $data['date'] = $request->date;

        return redirect()->route('checkout-second', $id)->with($data);

    }

    public function chooseTable($id)
    {

        if (Cart::count() == 0) {
            abort(404);
        }

        $data['menu'] = '2';
        $data['category'] = CategoryRestaurant::all();
        $data['id'] = $id;
        return view('main.checkout.index', $data);
    }

    public function getTable($id, Request $request)
    {

        $dateNow = date("d-m-Y");

        $val = DateTransaction::where('date', $dateNow)->first();
        if ($val == null) {
            $date_transaction = new DateTransaction();
            $date_transaction->date = $dateNow;
            $date_transaction->save();
            $id_date = $date_transaction->id;
        } else {
            $id_date = $val->id;
        }

        $time_reservation = TimeReservation::find($request->time);

        $data = new Transaction();
        $data->id_users = Auth::user()->id;
        $data->id_restaurants = $id;
        $data->id_date = $id_date;
        $data->id_time = $request->time;
        $data->id_tables = $request->table;
        $data->date_reservation = $request->date . " - " . $time_reservation->time;

        $data->save();

        return redirect()->route('checkout-third', $data->id);
    }

    public function checkout($id)
    {

        if ($id == null) {
            abort(404);
        }

        $data['menu'] = '3';
        $data['category'] = CategoryRestaurant::all();
        $data['transaction'] = Transaction::find($id);
        return view('main.checkout.index', $data);
    }

    public function createReservation($id, Request $request)
    {

        if ($id == null) {
            abort(404);
        }

        $data = Transaction::find($id);
        $data->name = $request->name;
        $data->email = Auth::user()->email;
        $data->phone = $request->phone;
        $data->notes = $request->notes;
        $data->total = Cart::total();
        $data->status = 'PENDING';
        $data->save();

        foreach (Cart::content() as $value) {
            $basket = new Basket();
            $basket->id_transactions = $id;
            $basket->id_products = $value->id;
            $basket->quantity = $value->qty;
            $basket->price = $value->price;
            $basket->total = $value->qty * $value->price;
            $basket->save();
        }

        Cart::destroy();

        return redirect()->route('home');
    }

    public function cancelReservation($id){
        Transaction::find($id)->delete();
        return redirect()->route('home');
    }

    public  function order(){
        $data['category'] = CategoryRestaurant::all();
        $data['order'] = Transaction::where('id_users',Auth::user()->id)->latest()->get();
        $data['basket'] = Basket::all();
        return view('main.order.index', $data);
    }
}

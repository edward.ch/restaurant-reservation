<?php

namespace App\Http\Controllers;

use App\CategoryProduct;
use App\CategoryRestaurant;
use App\Mail\AcceptEmail;
use App\Product;
use App\Tables;
use App\TimeReservation;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RestaurantController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard(){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();
        $data['transaction'] = Transaction::where('id_restaurants', Auth::user()->id)->get();
        $data['product'] = Product::where('id_restaurants', Auth::user()->id)->get();
        return view('superuser.partner.dashboard.index',$data);
    }

    public function configuration(){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['order']      = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();
        $data['user']       = User::find(Auth::user()->id);
        $data['category']   = CategoryRestaurant::all();

        return view('superuser.partner.configuration.index',$data);

    }

    public function configurationUpdate(Request $request){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $this->validate($request,[
            'gmap' => 'required'
        ]);

        $restaurant                 = User::find(Auth::user()->id);
        $restaurant->id_category    = $request->category;
        $restaurant->open_hours     = $request->open;
        $restaurant->close_hours    = $request->close;
        $restaurant->gmap_query     = $request->gmap;
        $restaurant->latitude       = $request->latitude;
        $restaurant->longitude      = $request->longitude;

        if ($request->file('image') == ""){
            $restaurant->image      = $restaurant->image;
        }else{
            $file                   = $request->file('image');
            $fileName               = 'RESTAURANT'.'_'.Auth::user()->id."_".$file->getClientOriginalName();

            $request->file('image')->move("image/avatar/", $fileName);

            $restaurant->image      = $fileName;
        }

        $restaurant->save();

        return redirect()->route('configuration-restaurant');

    }

    public function category(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }
        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();
        $data['category'] = CategoryProduct::where('id_restaurants', Auth::user()->id)->get();

        return view('superuser.partner.category.index',$data);
    }

    public function categoryCreate(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();


        return view('superuser.partner.category.content',$data);
    }

    public function categoryCreated(Request $request){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $this->validate($request,[
            'title' => 'required'
        ]);

        $data                   = new CategoryProduct();
        $data->id_restaurants   = Auth::user()->id;
        $data->title            = $request->title;

        $data->save();

        return redirect()->route('category-restaurant');
    }

    public function product(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['product'] = Product::where('id_restaurants', Auth::user()->id)->get();
        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();

        return view('superuser.partner.product.index',$data);
    }

    public function productCreate(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['category'] = CategoryProduct::where('id_restaurants', Auth::user()->id)->get();
        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();

        return view('superuser.partner.product.content',$data);
    }

    public function productCreated(Request $request){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $this->validate($request,[
            'title' => 'required',
            'price' => 'required|numeric|min:0',
            'desc' => 'required'
        ]);

        $data                   = new Product();
        $data->id_restaurants   = Auth::user()->id;
        $data->id_category      = $request->category;
        $data->title            = $request->title;
        $data->price            = $request->price;
        $data->desc             = $request->desc;
        $data->status           = 'AVAILABLE';

        $file                   = $request->file('image');
        $fileName               = 'PRODUCT'.'_'.Auth::user()->id."_".$file->getClientOriginalName();

        $request->file('image')->move("image/product/", $fileName);

        $data->image            = $fileName;

        $data->save();

        return redirect()->route('product-restaurant');
    }

    public function tables(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['tables'] = Tables::where('id_restaurants', Auth::user()->id)->get();
        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();

        return view('superuser.partner.tables.index',$data);
    }

    public function tablesCreate(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['tables'] = Tables::where('id_restaurants', Auth::user()->id)->get();
        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();

        return view('superuser.partner.tables.content',$data);
    }

    public function tablesCreated(Request $request){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $this->validate($request,[
            'number' => 'required|numeric|min:1',
            'title' => 'required'
        ]);

        $data                   = new Tables();
        $data->id_restaurants   = Auth::user()->id;
        $data->number_tables    = $request->number;
        $data->title            = $request->title;

        $data->save();

        return redirect()->route('product-restaurant');
    }

    public function order(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();

        return view('superuser.partner.order.index',$data);
    }

    public function orderAccept($id, Request $request){

        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        $data             = Transaction::find($id);
        $data->status     = 'ACCEPTED';
        $data->save();

        Mail::to($data->email)->send(new AcceptEmail());

        return redirect()->route('order-restaurant');
    }

    public function time(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();
        $data['time'] = TimeReservation::where('id_restaurants', Auth::user()->id)->get();

        return view('superuser.partner.time.index',$data);
    }

    public function timeCreate(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data['order'] = Transaction::where('id_restaurants', Auth::user()->id)->where('status','PENDING')->get();

        return view('superuser.partner.time.content',$data);
    }

    public function timeCreated(Request $request){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        $data = new TimeReservation();
        $data->id_restaurants = Auth::user()->id;
        $data->time = $request->time;

        $data->save();

        return redirect()->route('time-restaurant');
    }

    public function history(){
        if (Auth::user()->role != 'restaurant'){
            abort(404);
        }

        return view('superuser.partner.');
    }


}

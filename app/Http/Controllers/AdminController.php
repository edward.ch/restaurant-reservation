<?php

namespace App\Http\Controllers;

use App\CategoryRestaurant;
use App\News;
use App\Slider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['user'] = User::where('role','user')->count();
        $data['category'] = CategoryRestaurant::count();
        $data['restaurant'] = User::where('role','restaurant')->count();

        return view('superuser.admin.dashboard.index',$data);
    }

    public function user(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['user'] = User::where('role','user')->get();

        return view('superuser.admin.user.index',$data);
    }

    public function updateAdmin($id ,Request $request){

        if (Auth::user()->role == 'user'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required'
        ]);

        $admin           = User::find($id);
        $admin->name     = $request->name;
        $admin->email    = $admin->email;

        $admin->save();

        if (Auth::user()->role == 'admin'){
            return redirect()->route('dashboard-admin');
        }else{
            return redirect()->route('dashboard-restaurant');
        }

    }

//      =========================================== RESTAURANT ===========================================

    public function restaurant(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['restaurant'] = User::where('role','restaurant')->get();

        return view('superuser.admin.partner.index',$data);

    }

    public function restaurantCreate(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['category'] = CategoryRestaurant::all();

        return view('superuser.admin.partner.content',$data);

    }

    public function restaurantCreated(Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
            'gmap' => 'required'
        ]);

        $restaurant                 = new User();
        $restaurant->id_category    = $request->category;
        $restaurant->name           = $request->name;
        $restaurant->email          = $request->email;
        $restaurant->password       = Hash::make($request->password);
        $restaurant->role           = 'restaurant';
        $restaurant->image          = 'default.jpg';
        $restaurant->open_hours     = $request->open;
        $restaurant->close_hours    = $request->close;
        $restaurant->gmap_query     = $request->gmap;
        $restaurant->latitude       = $request->latitude;
        $restaurant->longitude      = $request->longitude;

        $restaurant->save();

        return redirect()->route('restaurant-admin');

    }

    public function restaurantUpdate($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        return view('superuser.admin');

    }

    public function restaurantUpdated($id, Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        $this->validate($request,[
            '' => 'required'
        ]);

    }

    public function restaurantDelete($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

    }

//      =========================================== CATEGORY ===========================================

    public function category(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['category'] = CategoryRestaurant::all();

        return view('superuser.admin.category.index',$data);

    }

    public function categoryCreate(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        return view('superuser.admin.category.content');

    }

    public function categoryCreated(Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $this->validate($request,[
            'title' => 'required'
        ]);

        $category = new CategoryRestaurant();
        $category->title = $request->title;

        $category->save();

        return redirect()->route('category-admin');

    }

    public function categoryUpdate($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        return view('superuser.admin');

    }

    public function categoryUpdated($id, Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        $this->validate($request,[
            '' => 'required'
        ]);

    }

    public function categoryDelete($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

    }

//      =========================================== SLIDER ===========================================

    public function slider(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['slider'] = Slider::all();

        return view('superuser.admin.slider.index',$data);

    }

    public function sliderCreate(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        return view('superuser.admin.slider.content');

    }

    public function sliderCreated(Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $this->validate($request,[
            'title' => 'required'
        ]);

        $data                   = new Slider();
        $data->title            = $request->title;

        $file                   = $request->file('image');
        $fileName               = 'SLIDER'.'_'.Auth::user()->id."_".$file->getClientOriginalName();

        $request->file('image')->move("image/slider/", $fileName);

        $data->image            = $fileName;

        $data->save();

        return redirect()->route('slider-admin');

    }

    public function sliderUpdate($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        return view('superuser.admin');

    }

    public function sliderUpdated($id, Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        $this->validate($request,[
            '' => 'required'
        ]);

    }

    public function sliderDelete($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

    }

//      =========================================== NEWS ===========================================

    public function news(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $data['news'] = News::all();

        return view('superuser.admin.news.index',$data);

    }

    public function newsCreate(){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        return view('superuser.admin.news.content');

    }

    public function newsCreated(Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        $this->validate($request,[
            'title' => 'required',
            'desc' => 'required'
        ]);

        $data                   = new News();
        $data->title            = $request->title;
        $data->posted           = Auth::user()->name;
        $data->date             = date("F j, Y");
        $data->desc             = $request->desc;

        $file                   = $request->file('image');
        $fileName               = 'NEWS'.'_'.Auth::user()->id."_".$file->getClientOriginalName();

        $request->file('image')->move("image/news/", $fileName);

        $data->image            = $fileName;

        $data->save();

        return redirect()->route('news-admin');

    }

    public function newsUpdate($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        return view('superuser.admin');

    }

    public function newsUpdated($id, Request $request){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

        $this->validate($request,[
            '' => 'required'
        ]);

    }

    public function newsDelete($id){

        if (Auth::user()->role != 'admin'){
            abort(404);
        }

        if ($id == null){
            abort(404);
        }

    }

}

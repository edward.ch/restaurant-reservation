<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    public function tables(){
        return $this->hasOne('App\Tables', 'id', 'id_tables');
    }

    public function restaurant(){
        return $this->hasOne('App\User', 'id', 'id_restaurants');
    }

}

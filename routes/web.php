<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/loading', 'HomeController@load')->name('load');
Route::get('logout',    'Auth\LoginController@logout', function () {
        return abort(404);
    });
Route::get('superuser/login', function () {
    return view('superuser.login');
})->name('superuser-login');
Route::get('superuser/', function () {
    return redirect()->route('superuser-login');
});


//      =========================================== HOMEPAGE ===========================================

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about-us', 'HomeController@about')->name('about');
Route::get('/restaurant', 'HomeController@listRestaurant')->name('restaurant');
Route::get('/restaurant/category/{id}', 'HomeController@categoryRestaurant')->name('category-list-restaurant');
Route::get('/restaurant/detail/{id}', 'HomeController@detailRestaurant')->name('detail-restaurant');
Route::get('/news', 'HomeController@listNews')->name('news');
Route::get('/news/detail', 'HomeController@detailNews')->name('detail-news');
Route::get('/my-profile', 'HomeController@profile')->name('profile');
Route::get('/order', 'ReservationController@order')->name('order');

Route::get('/add-cart/{id}', 'ReservationController@addCart')->name('add-cart');
Route::get('/remove-cart/{rowId}', 'ReservationController@removeCart')->name('remove-cart');
Route::post('/filter-table/{id}', 'ReservationController@filterTable')->name('filter-table');
Route::get('/get-table/{id}', 'ReservationController@getTable')->name('get-table');
Route::get('/checkout/date/{id}', 'ReservationController@chooseDate')->name('checkout-first');
Route::get('/checkout/table/{id}', 'ReservationController@chooseTable')->name('checkout-second');
Route::get('/checkout/confirmation/{id}', 'ReservationController@checkout')->name('checkout-third');
Route::post('/create/reservation/{id}', 'ReservationController@createReservation')->name('create-reservation');
Route::get('/cancel/reservation/{id}', 'ReservationController@cancelReservation')->name('cancel-reservation');

//      =========================================== ADMIN ===========================================

Route::get('/admin/dashboard', 'AdminController@dashboard')->name('dashboard-admin');
Route::post('/admin/update/{id}', 'AdminController@updateAdmin')->name('update-admin');
Route::get('/admin/restaurant', 'AdminController@restaurant')->name('restaurant-admin');
Route::get('/admin/category', 'AdminController@category')->name('category-admin');
Route::get('/admin/slider', 'AdminController@slider')->name('slider-admin');
Route::get('/admin/news', 'AdminController@news')->name('news-admin');
Route::get('/admin/user', 'AdminController@user')->name('user-admin');

Route::get('/admin/restaurant/create', 'AdminController@restaurantCreate')->name('create-restaurant-admin');
Route::post('/admin/restaurant/created/', 'AdminController@restaurantCreated')->name('created-restaurant-admin');
Route::post('/admin/restaurant/delete/{id}', 'AdminController@restaurantDelete')->name('delete-restaurant-admin');

Route::get('/admin/category/create', 'AdminController@categoryCreate')->name('create-category-admin');
Route::post('/admin/category/created', 'AdminController@categoryCreated')->name('created-category-admin');
Route::get('/admin/category/update/{id}', 'AdminController@categoryUpdate')->name('update-category-admin');
Route::post('/admin/category/updated/{id}', 'AdminController@categoryUpdated')->name('updated-category-admin');
Route::post('/admin/category/delete/{id}', 'AdminController@categoryDelete')->name('delete-category-admin');

Route::get('/admin/slider/create', 'AdminController@sliderCreate')->name('create-slider-admin');
Route::post('/admin/slider/created', 'AdminController@sliderCreated')->name('created-slider-admin');
Route::get('/admin/slider/update/{id}', 'AdminController@sliderUpdate')->name('update-slider-admin');
Route::post('/admin/slider/updated/{id}', 'AdminController@sliderUpdated')->name('updated-slider-admin');
Route::post('/admin/slider/delete/{id}', 'AdminController@sliderDelete')->name('delete-slider-admin');

Route::get('/admin/news/create', 'AdminController@newsCreate')->name('create-news-admin');
Route::post('/admin/news/created', 'AdminController@newsCreated')->name('created-news-admin');
Route::get('/admin/news/update/{id}', 'AdminController@newsUpdate')->name('update-news-admin');
Route::post('/admin/news/updated/{id}', 'AdminController@newsUpdated')->name('updated-news-admin');
Route::post('/admin/news/delete/{id}', 'AdminController@newsDelete')->name('delete-news-admin');

//      =========================================== RESTAURANT ===========================================

Route::get('/partner/dashboard', 'RestaurantController@dashboard')->name('dashboard-restaurant');
Route::get('/partner/product', 'RestaurantController@product')->name('product-restaurant');
Route::get('/partner/tables', 'RestaurantController@tables')->name('tables-restaurant');
Route::get('/partner/category', 'RestaurantController@category')->name('category-restaurant');
Route::get('/partner/order', 'RestaurantController@order')->name('order-restaurant');
//Route::get('/partner/order/detail/{id}', 'RestaurantController@orderDetail')->name('detail-order-restaurant');
Route::get('/partner/history/transaction', 'RestaurantController@historyTransaction')->name('history-restaurant');
Route::get('/partner/slider', 'RestaurantController@slider')->name('slider-restaurant');
Route::get('/partner/time', 'RestaurantController@time')->name('time-restaurant');
Route::get('/partner/configuration', 'RestaurantController@configuration')->name('configuration-restaurant');
Route::post('/partner/configuration/update', 'RestaurantController@configurationUpdate')->name('update-config-restaurant');

Route::get('/partner/order/accept/{id}', 'RestaurantController@orderAccept')->name('order-accept-restaurant');
Route::get('/partner/order/remove/{id}', 'RestaurantController@orderRemove')->name('order-remove-restaurant');

Route::get('/partner/product/create', 'RestaurantController@productCreate')->name('create-product-restaurant');
Route::post('/partner/product/created', 'RestaurantController@productCreated')->name('created-product-restaurant');
Route::get('/partner/product/update/{id}', 'RestaurantController@productUpdate')->name('update-product-restaurant');
Route::post('/partner/product/updated/{id}', 'RestaurantController@productUpdated')->name('updated-product-restaurant');
Route::post('/partner/product/delete/{id}', 'RestaurantController@productDelete')->name('delete-product-restaurant');

Route::get('/partner/category/create', 'RestaurantController@categoryCreate')->name('create-category-restaurant');
Route::post('/partner/category/created', 'RestaurantController@categoryCreated')->name('created-category-restaurant');
Route::get('/partner/category/update/{id}', 'RestaurantController@categoryUpdate')->name('update-category-restaurant');
Route::post('/partner/category/updated/{id}', 'RestaurantController@categoryUpdated')->name('updated-category-restaurant');
Route::post('/partner/category/delete/{id}', 'RestaurantController@categoryDelete')->name('delete-category-restaurant');

Route::get('/partner/tables/create', 'RestaurantController@tablesCreate')->name('create-tables-restaurant');
Route::post('/partner/tables/created', 'RestaurantController@tablesCreated')->name('created-tables-restaurant');
Route::get('/partner/tables/update/{id}', 'RestaurantController@tablesUpdate')->name('update-tables-restaurant');
Route::post('/partner/tables/updated/{id}', 'RestaurantController@tablesUpdated')->name('updated-tables-restaurant');
Route::post('/partner/tables/delete/{id}', 'RestaurantController@tablesDelete')->name('delete-tables-restaurant');

Route::get('/partner/slider/create', 'RestaurantController@sliderCreate')->name('create-slider-restaurant');
Route::post('/partner/slider/created', 'RestaurantController@sliderCreated')->name('created-slider-restaurant');
Route::get('/partner/slider/update/{id}', 'RestaurantController@sliderUpdate')->name('update-slider-restaurant');
Route::post('/partner/slider/updated/{id}', 'RestaurantController@sliderUpdated')->name('updated-slider-restaurant');
Route::post('/partner/slider/delete/{id}', 'RestaurantController@sliderDelete')->name('delete-slider-restaurant');

Route::get('/partner/time/create', 'RestaurantController@timeCreate')->name('create-time-restaurant');
Route::post('/partner/time/created', 'RestaurantController@timeCreated')->name('created-time-restaurant');
Route::get('/partner/time/update/{id}', 'RestaurantController@timeUpdate')->name('update-time-restaurant');
Route::post('/partner/time/updated/{id}', 'RestaurantController@timeUpdated')->name('updated-time-restaurant');
Route::post('/partner/time/delete/{id}', 'RestaurantController@timeDelete')->name('delete-time-restaurant');
